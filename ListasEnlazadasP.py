'''
Created on 30 oct. 2020

@author: Denise
'''

class Nodo:
    def __init__(self, dato):
        if dato is None:
            self.__dato = int()
        else:
            self.__dato = dato
        self.__nodoSiguiente = None
        
    def getDato(self):
        return self.__dato
    def setDato(self, dato):
        self.__dato=dato
    
    def getNodoSiguiente(self):
        return self.__nodoSiguiente
    def setNodoSiguiente(self, nodoSiguiente):
        self.__nodoSiguiente=nodoSiguiente
    
    def __str__(self):
        return ("Nodo [dato=" + str(self.getDato()) + ", nodoSiguiente=" + str(self.getNodoSiguiente()) + "]")
    
class ListaEnlazada:
    def __init__(self):
        self.nodoInicio = None
        self.nodoFin = None
        
    def listaVacia(self):
        return ((self.nodoInicio==None)and(self.nodoFin==None))
    
    def agregarElementoAlInicio(self, dato):
        nodoNuevo = Nodo(dato)
        if self.listaVacia():
            self.nodoInicio=nodoNuevo
            self.nodoFin=Nodo
        else:
            nodoNuevo.setNodoSiguiente(self.nodoInicio)
            self.nodoInicio=nodoNuevo     
                
    def agregarElementoAlFinal(self, dato):
        nn = Nodo(dato)
        if self.listaVacia():
            self.agregarElementoAlInicio(dato)
        else:
            nodoActual = self.nodoInicio
            while(nodoActual.getNodoSiguiente()!=None):
                nodoActual=nodoActual.getNodoSiguiente()
            nodoActual.setNodoSiguiente(nn)
    
    def eliminarDatoInicio(self):
        if(self.listaVacia()):
            return (-1)
        else:
            try:
                nodoActual = self.nodoInicio
                ret = nodoActual.getDato()
                self.nodoInicio=nodoActual.getNodoSiguiente()
                return ret
            except:
                return (-1)
            
    def eliminarDatoFinal(self):
        if (self.listaVacia()):
            return (-1)
        else:
            try:
                nodoAnterior = self.nodoInicio
                nodoSiguiente = self.nodoInicio.getNodoSiguiente()
                if (nodoSiguiente==None):
                    ret = self.nodoInicio.getDato()
                    self.nodoInicio=None
                    self.nodoFin=None
                    return ret
                else:
                    while(nodoSiguiente.getNodoSiguiente()!=None):
                        nodoAnterior = nodoAnterior.getNodoSiguiente()
                        nodoSiguiente = nodoSiguiente.getNodoSiguiente()
                    ret = nodoSiguiente.getDato()
                    nodoAnterior.setNodoSiguiente(None)
                    return ret
            except:
                return (-1)
            
    def eliminarDatoEspecifico(self, dato):
        if (self.nodoInicio==None):
            return (-1)
        elif((self.nodoInicio==self.nodoFin)and(self.nodoInicio.getDato()==dato)):
            print("encontrado en el primer NODO")
            n = self.nodoInicio.getDato()
            self.nodoInicio=self.nodoInicio.getNodoSiguiente()
            self.nodoFin=self.nodoInicio
            return n
        else:
            nodoAnterior = self.nodoInicio
            nodoSiguiente = self.nodoInicio.getNodoSiguiente()
            
            if ((nodoAnterior!=None)and(nodoAnterior.getDato()==dato)):
                n = nodoAnterior.getDato()
                self.nodoInicio=nodoAnterior.getNodoSiguiente();
                return n
                
            else:
                while((nodoSiguiente!=None)and(nodoSiguiente.getDato()!=dato)):
                    nodoAnterior = nodoAnterior.getNodoSiguiente()
                    nodoSiguiente = nodoSiguiente.getNodoSiguiente()
                
                if ((nodoSiguiente!=None)and(nodoSiguiente.getDato()==dato)):
                    n = nodoSiguiente.getDato()
                    nodoSiguiente = nodoSiguiente.getNodoSiguiente()
                    nodoAnterior.setNodoSiguiente(nodoSiguiente)
                    return n
                else:
                    return (-99999)
    
    
    
    def mostrarCantidadElementos(self):
        nodoActual = self.nodoInicio
        cc=0
        while(nodoActual!=None):
            cc+=1
            nodoActual=nodoActual.getNodoSiguiente()
        print("Cantidad de elementos: "+str(cc))
    
    def mostrarElementos(self):
        nodoActual=self.nodoInicio
        while(nodoActual!=None):
            print("["+str(nodoActual.getDato())+"]-->",end="")
            nodoActual=nodoActual.getNodoSiguiente()
        print()
        
opc=0
dato=0
num=0
miLe = ListaEnlazada()


miLe.agregarElementoAlInicio(9)
miLe.agregarElementoAlInicio(10)
miLe.agregarElementoAlInicio(11)
miLe.agregarElementoAlInicio(13)
miLe.agregarElementoAlInicio(15)
miLe.agregarElementoAlInicio(5)

miLe.mostrarElementos()

num = miLe.eliminarDatoEspecifico(11)

if(num==(-1)):
    
    print("Lista Vacia")
elif(num==(-99999)):
    print("No se encontro el dato")
else:
    print(" se elimino correctamente")
    
miLe.mostrarElementos()

miLe.agregarElementoAlFinal(18)

miLe.mostrarElementos()

miLe.eliminarDatoInicio()
miLe.mostrarElementos()
miLe.eliminarDatoFinal()
miLe.mostrarElementos()

miLe.mostrarCantidadElementos()
